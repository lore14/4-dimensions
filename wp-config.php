<?php
/**
 * Il file base di configurazione di WordPress.
 *
 * Questo file viene utilizzato, durante l’installazione, dallo script
 * di creazione di wp-config.php. Non è necessario utilizzarlo solo via
 * web, è anche possibile copiare questo file in «wp-config.php» e
 * riempire i valori corretti.
 *
 * Questo file definisce le seguenti configurazioni:
 *
 * * Impostazioni MySQL
 * * Prefisso Tabella
 * * Chiavi Segrete
 * * ABSPATH
 *
 * È possibile trovare ultetriori informazioni visitando la pagina del Codex:
 *
 * @link https://codex.wordpress.org/it:Modificare_wp-config.php
 *
 * È possibile ottenere le impostazioni per MySQL dal proprio fornitore di hosting.
 *
 * @package WordPress
 */

// ** Impostazioni MySQL - È possibile ottenere queste informazioni dal proprio fornitore di hosting ** //
/** Il nome del database di WordPress */
define('DB_NAME', '4-dimensions-wp');

/** Nome utente del database MySQL */
define('DB_USER', 'root');

/** Password del database MySQL */
define('DB_PASSWORD', '');

/** Hostname MySQL  */
define('DB_HOST', 'localhost');

/** Charset del Database da utilizzare nella creazione delle tabelle. */
define('DB_CHARSET', 'utf8mb4');

/** Il tipo di Collazione del Database. Da non modificare se non si ha idea di cosa sia. */
define('DB_COLLATE', '');

define('FS_METHOD', 'direct');
/**#@+
 * Chiavi Univoche di Autenticazione e di Salatura.
 *
 * Modificarle con frasi univoche differenti!
 * È possibile generare tali chiavi utilizzando {@link https://api.wordpress.org/secret-key/1.1/salt/ servizio di chiavi-segrete di WordPress.org}
 * È possibile cambiare queste chiavi in qualsiasi momento, per invalidare tuttii cookie esistenti. Ciò forzerà tutti gli utenti ad effettuare nuovamente il login.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '5ECN@)pdDR6n0vWL|]iJi5_[)Js/mJ?[$;<e *|z:t /Mr@Lzi2o kLQH]w-MSw@');
define('SECURE_AUTH_KEY',  '~ASE )o4feSl7a$^pR(K26ULJ7/1}s:v;B</_Ap6a/xHkel+WcH`hs!pg<Az}(4a');
define('LOGGED_IN_KEY',    'R gpIZ]Ni(Nw&2)G96ORWO+6P==zw|}q2vw,~`-sMY38=Be4HOfK>+Kf1nO6<Ch(');
define('NONCE_KEY',        'yGW`I(YZqy)/Co,TmPzt&*f<n(kTgV].B&RjBHPC9W$ec`=9pJtz|}~1oG@f~Mm@');
define('AUTH_SALT',        '6+UT=zij|@YaG^ZbAH i7{3_&-;/c2cIop<zmF?+/{:6^S+aVc&3*?E:&;y)($|t');
define('SECURE_AUTH_SALT', 'T0wt~^XNG46QtU?UQ|JZdgM2T~nO+zkf8,s}yf0&0Z!6S` [I[hn)fXfh0q{eSI=');
define('LOGGED_IN_SALT',   '(0w2Bh^pKF;XwP +fLsssRJRZSBM~8~Y4-6a,.=u^O&==!wieF@WuvhC%OWDEb<}');
define('NONCE_SALT',       'V1wBZ(p/p7+a{z~m$cs(L{<v,8l)0>-{*e01/JNvadA1m!ur(n;sXB{Sn/:&.!mg');

/**#@-*/

/**
 * Prefisso Tabella del Database WordPress.
 *
 * È possibile avere installazioni multiple su di un unico database
 * fornendo a ciascuna installazione un prefisso univoco.
 * Solo numeri, lettere e sottolineatura!
 */
$table_prefix  = 'wp_';

/**
 * Per gli sviluppatori: modalità di debug di WordPress.
 *
 * Modificare questa voce a TRUE per abilitare la visualizzazione degli avvisi
 * durante lo sviluppo.
 * È fortemente raccomandato agli svilupaptori di temi e plugin di utilizare
 * WP_DEBUG all’interno dei loro ambienti di sviluppo.
 */
define('WP_DEBUG', false);

/* Finito, interrompere le modifiche! Buon blogging. */

/** Path assoluto alla directory di WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Imposta le variabili di WordPress ed include i file. */
require_once(ABSPATH . 'wp-settings.php');
