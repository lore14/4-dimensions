��          t      �              !        ?     K     X  
   u  P   �  �   �     �     �  z  �     s  7   �     �     �  A   �        }   6  �  �     y     �                                               	   
          Choose File Help us improve your experience:  Image Sizes Nazmul Ahsan Okay, don't bother me again! Select All So, it creates multiple sizes of an image while uploading? Here is the solution! We want to know what types of sites use our plugin. So that we can improve <i>Image Sizes</i> accordingly. Help us with your site URL and few basic information. It doesn't include your password or any kind of sercret data. Would you like to help us? http://nazmulahsan.me https://codebanyan.com PO-Revision-Date: 2017-06-17 04:39:57+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: GlotPress/2.4.0-alpha
Language: ru
Project-Id-Version: Plugins - Stop Generating Image Sizes - Development (trunk)
 Выберите файл Помогите нам улучшить плагин:  Image Sizes Nazmul Ahsan Хорошо, но не беспокойте меня снова! Выбрать все При загрузке создается несколько размеров изображения? Вот решение! Мы хотим знать, какие типы сайтов используют наш плагин. Таким образом, мы можем улучшить <i>Image Sizes</i>. Помогите нам, указав URL вашего сайта и немного основной информации. Она не включает ваш пароль или любые другие секретные данные. Хотите нам помочь? http://nazmulahsan.me https://codebanyan.com 