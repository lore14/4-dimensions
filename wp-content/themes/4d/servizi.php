<?php
/*
 Template Name: servizi
*/
  get_header();
?>

<div class="main-content">
  <div class="container">
	<?php if (have_posts()) :?><?php while(have_posts()) : the_post(); ?>

		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

			<div class="row">
				<div class="col-sm-12">
					<?php the_post_thumbnail('', array('class' => 'img-fluid','alt' => get_the_title())); ?>
				</div>
			</div>

    <!--  INIZIO TITOLO E "ARTICOLO"  -->
			<div class="row m_top10">
				<div class="col-sm-12 text-center">
					<h1 class="upp"><?php the_title(); ?></h1>
					<h2 class="subline"><?php the_content();?></h2>
				</div>
			</div>


    <div class="about">
  		<div class="container">
  			<div class="row">
  				<div class="col-sm-6 specifiche">
              <ul class="text-right">
                <li><?php esc_html_e ('architectural consultation', 'qd') ?></li>
                <li><?php esc_html_e ('architectural design', 'qd') ?></li>
                <li><?php esc_html_e ('urban design', 'qd') ?></li>
                <li><?php esc_html_e ('construction supervision', 'qd') ?></li>
                <li><?php esc_html_e ('project management', 'qd') ?></li>
                <li><?php esc_html_e ('landscape project', 'qd') ?></li>
              </ul>
          </div>
          <div class="col-sm-6">
            <a id="architecture"></a>
            <hr>
            <h3 class="text-right"><b><?php esc_html_e ( 'ARCHITECTURE', 'qd') ?></b></h3>
            <p><?php esc_html_e ( 'Lo Studio Four Dimensions &egrave; costituito da un team di professionisti di pluriennale esperienza, sia nel campo della progettazione ex-novo che in quello della valorizzazione e ristrutturazione di edifici e strutture gi&agrave; esistenti. Le proposte progettuali della societ&agrave; vogliono sempre essere in linea con l&rsquo;evolversi dell&rsquo;architettura contemporanea, e allo stesso tempo rispettose della storia e del contesto all&rsquo;interno del quale il singolo progetto si colloca: ogni architettura non pu&ograve; che nascere in rapporto a un particolare ambiente, attento ai temi della sostenibilit&agrave;, con l&rsquo;imprescindibile obiettivo di realizzare soluzioni che durino nel tempo. La continua ricerca di innovazione tecnologica verte anche allo studio di materiali innovativi, con lo scopo di suggerire nuove soluzione e nuove opportunit&agrave; di impiego degli stessi. Il successo della societ&agrave; si fonda inoltre sulla capacit&agrave; di soddisfare le esigenze dei clienti in tempi rapidi, fornendo un servizio completo, affidabile ed indipendente.', 'qd') ?>
            </p>
          </div>
        </div>
      </div>
    </div>


    <div class="about">
  		<div class="container">
  			<div class="row">
  				<div class="col-sm-6 d-lg-none d-md-none specifiche">
            <ul>
              <li>Space Planning</li>
              <li>Re-Planning</li>
              <li>Furniture Production</li>
              <li>Furniture Accessories</li>
              <li>Lighting Design</li>
              <li>Garden Design</li>
            </ul>
          </div>
          <div class="col-sm-6">
            <a id="design"></a>
            <hr>
            <h3 class="text-left"><b><?php esc_html_e ( 'INTERIOR DESIGN', 'qd') ?></b></h3>
            <p><?php esc_html_e ( 'Four Dimensions &egrave; anche professionalit&agrave; ed esperienza nel creare soluzioni per interni unici e confortevoli, sempre in simbiosi con le esigenze del committente. Ogni ambiente viene studiato su misura, personalizzato e definito in ogni dettaglio, per raggiungere un perfetto equilibrio di praticit&agrave; e comfort.', 'qd')?>
              <br><br>
               <?php esc_html_e( 'Oltre allo studio di progetto e all&rsquo; allestimento, la societ&agrave; segue il cliente nella scelta di abbinamenti cromatici e nella cura dei particolari , completando ogni abitazione con particolari che la caratterizzano, rendendola unica ed esclusiva.', 'qd') ?>
            </p>
          </div>
          <div class="col-sm-6 d-none d-lg-block d-md-block d-sm-none specifiche">
            <ul>
              <li><?php esc_html_e ('Space Planning', 'qd') ?></li>
              <li><?php esc_html_e ('Re-Planning', 'qd') ?></li>
              <li><?php esc_html_e ('Furniture Production', 'qd') ?></li>
              <li><?php esc_html_e ('Furniture Accessories', 'qd') ?></li>
              <li><?php esc_html_e ('Lighting Design', 'qd') ?></li>
              <li><?php esc_html_e ('Garden Design', 'qd') ?></li>
            </ul>
          </div>
        </div>
      </div>
    </div>

    <div class="about">
      <div class="container">
        <div class="row">
          <div class="col-sm-6 specifiche">
            <ul class="text-right">
              <li><?php esc_html_e ('Turn-Key Project Delivered', 'qd') ?></li>
              <li><?php esc_html_e ('Fit Out Installation', 'qd') ?></li>
              <li><?php esc_html_e ('General Supervision', 'qd') ?></li>
              <li><?php esc_html_e ('Logistics', 'qd') ?></li>
              <li><?php esc_html_e ('Management Assistance', 'qd') ?></li>
            </ul>
          </div>
          <div class="col-sm-6">
            <a id="contract"></a>
            <hr>
            <h3 class="text-right"><b><?php esc_html_e ( 'CONTRACT', 'qd') ?></b></h3>
            <p><?php esc_html_e ( 'L&rsquo;ufficio tecnico di Four Dimensions ha una mission precisa e definita: interpretare le esigenze dei clienti e realizzare con loro progetti e soluzioni uniche, che si adattino alle specifiche e singole esigenze, per soddisfare gusti e necessit&agrave;, senza mai perdere di vista originalit&agrave;, qualit&agrave; e stile.', 'qd') ?>
              <br><br>
              <?php esc_html_e ( 'Ogni fase del progetto, a partire dall&rsquo;ideazione, fino ad arrivare alla realizzazione e alla definitiva consegna, &egrave; seguita con attenzione in ogni singolo dettaglio, coordinata e supervisionata da un project manager, che ha il compito di portare a soddisfazione ogni pi&ugrave; piccolo desiderio dei clienti.', 'qd') ?>
              <br><br>
              <?php esc_html_e ( 'La professionalit&agrave; e la precisione con cui vengono svolte tutte le attivit&agrave; in studio sono garantite da un team di lavoro altamente qualificato, che unisce all&rsquo;esperienza nel settore una grande passione per il proprio lavoro.', 'qd') ?>
            </p>
          </div>
        </div>
      </div>
    </div>


    <div class="about">
  		<div class="container">
  			<div class="row">
          <div class="col-sm-6 d-lg-none d-md-none specifiche">
            <ul>
              <li><?php esc_html_e ('Strategic Consulting', 'qd') ?></li>
              <li><?php esc_html_e ('Solutions Planning', 'qd') ?></li>
              <li><?php esc_html_e ('Development Strategy', 'qd') ?></li>
              <li><?php esc_html_e ('Process Management', 'qd') ?></li>
              <li><?php esc_html_e ('Budget Estimations', 'qd') ?></li>
              <li><?php esc_html_e ('Event Consultancy', 'qd') ?></li>
            </ul>
          </div>
          <div class="col-sm-6">
            <a id="consulting"></a>
            <hr>
            <h3 class="text-left"><b><?php esc_html_e ( 'CONSULTING', 'qd') ?></b></h3>
            <p><?php esc_html_e ( 'Four Dimensions offre ai suoi clienti servizi di consulenza strategica, volti a identificare e pianificare progetti e soluzioni, in una collaborazione che si fonda sull&rsquo;esperienza e, allo stesso tempo, sull&rsquo;innovazione e sulla ricerca costante di idee straordinarie.', 'qd')?>
              <br><br>
              <?php esc_html_e( 'I servizi di consulenza sono finalizzati alla progettazione e alla concreta riuscita di esiti ottimali, in grado di aiutare nella realizzazione di architetture e spazi.', 'qd')?>
              <br><br>
              <?php esc_html_e( 'Attraverso una struttura professionalmente qualificata, con collaboratori responsabili che intervengono a fianco del cliente quando &egrave; necessario prendere decisioni appropriate e metterle in pratica con efficacia, Four Dimensions si pone come obiettivo il raggiungimento della soddisfazione del cliente e, allo stesso tempo, personale.', 'qd') ?>
            </p>
          </div>
          <div class="col-sm-6 d-none d-lg-block d-md-block d-sm-none specifiche">
            <ul>
              <li><?php esc_html_e ('Strategic Consulting', 'qd') ?></li>
              <li><?php esc_html_e ('Solutions Planning', 'qd') ?></li>
              <li><?php esc_html_e ('Development Strategy', 'qd') ?></li>
              <li><?php esc_html_e ('Process Management', 'qd') ?></li>
              <li><?php esc_html_e ('Budget Estimations', 'qd') ?></li>
              <li><?php esc_html_e ('Event Consultancy', 'qd') ?></li>
            </ul>
          </div>
        </div>
      </div>
    </div>

		</article>

	<?php endwhile; ?>

		<div class="pagination">

			<?php /* Pagination */
			global $wp_query;
			$big = 999999999; // need an unlikely integer
			echo paginate_links( array(
				'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
				'format' => '?paged=%#%',
				'current' => max( 1, get_query_var('paged') ),
				'total' => $wp_query->max_num_pages
			) );
			?>

		</div>

		<?php else : ?>

		  <h3> <?php esc_html_e('Spiacente, non ci sono post per la tua ricerca.', 'qd'); ?> </h3>

		<?php endif; ?>


  </div>
</div>

<?php get_footer(); ?>
