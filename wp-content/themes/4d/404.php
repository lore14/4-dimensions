<?php get_header(); ?>

<main class="container mt-2 main-content">

  <div class="row">
    <div class="col-sm-8 ml-sm-auto mr-sm-auto">

      <article class="mb-5">
        <h1 class="display-4 mb-5 text-center">
          <?php esc_html_e( 'La pagina cercata non esiste', 'qd' ); ?>
        </h1>
        <h2 class="text-center display-3">
            <?php esc_html_e( 'Errore 404', 'qd' ); ?>
        </h2>
      </article>

    </div>
</main>


<?php get_footer(); ?>
