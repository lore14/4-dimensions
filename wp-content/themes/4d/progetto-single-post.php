<?php
/*
 * Template Name: Template progetto
 * Template Post Type: post
 */

 get_header();
?>

<div class="main-content container m_top">
	<?php if (have_posts()) :?><?php while(have_posts()) : the_post(); ?>

		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

			<div class="row">
				<div class="col-sm-12">
          <div class="project_det">
            <h3 class="upp"><?php the_title(); ?></h3>
            <h5>
              <?php $luogo = get_post_custom_values( 'luogo' );
              foreach ( $luogo as $key => $value ) {
                echo "$value";
              } ?>
            </h5>
          </div>
        </div>
        <div class="col-sm-12 m_bot">
          <?php $img = get_post_custom_values( 'wpbgallery_gallery' );
          foreach ( $img as $key => $value ) {
          }?>

          <img src="<?php echo $value; ?>" alt="">


	      </div>

    <div class="container">
			<div class="row m_top">
				<div class="col-sm-12">
					<?php the_content();?>
				</div>
			</div>
		</div>

		</article>

	<?php endwhile; ?>

		<div class="pagination">

			<?php /* Pagination */
			global $wp_query;
			$big = 999999999; // need an unlikely integer
			echo paginate_links( array(
				'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
				'format' => '?paged=%#%',
				'current' => max( 1, get_query_var('paged') ),
				'total' => $wp_query->max_num_pages
			) );
			?>

		</div>

	<?php else : ?>

	  <h3> <?php esc_html_e('Spiacente, non ci sono post per la tua ricerca.', 'qd'); ?> </h3>

	<?php endif; ?>

  </div>
</div>


<?php get_footer(); ?>
