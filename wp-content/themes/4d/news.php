<?php
/*
 Template Name: news
*/
  get_header();
?>

<div class="main-content container">

	<?php
		$catquery = new WP_Query('category_name=news');
		if (have_posts()) :	while($catquery->have_posts()) : $catquery->the_post();
	?>

		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

			<div class="row m_top10">
				<div class="col-sm-12">
					<h1 class="upp display-4 text-center"><?php the_title(); ?></h1>
					<p><?php the_time('j M , Y') ?> <?php //the_category(', '); ?></p>

					<a href="<?php the_permalink(); ?>">
						<?php the_post_thumbnail('', array('class' => 'img-fluid','alt' => get_the_title())); ?>
					</a>

					<div class="pre-text mt-2" ><?php the_excerpt();?></div>

						<h2 class="subline">
							<a href="<?php the_permalink(); ?>"><?php esc_html_e( 'Leggi di più...' , 'qd' ); ?></a>
						</h2>

				</div>

			</div>

		</article>

	<?php endwhile;
			wp_reset_postdata();
	?>

		<div class="pagination">

			<?php /* Pagination */
			global $wp_query;
			$big = 999999999; // need an unlikely integer
			echo paginate_links( array(
				'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
				'format' => '?paged=%#%',
				'current' => max( 1, get_query_var('paged') ),
				'total' => $wp_query->max_num_pages
			) );
			?>

		</div>

	<?php else : ?>

	  <h3> <?php esc_html_e('Spiacente, non ci sono post per la tua ricerca.', 'qd'); ?> </h3>

	<?php endif; ?>

</div>

<aside class="sidebar">
		<?php get_sidebar(); ?>
</aside>


<?php get_footer(); ?>
