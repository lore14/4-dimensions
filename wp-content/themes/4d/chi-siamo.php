<?php
/*
 Template Name: chi-siamo
*/
  get_header();
?>

<div class="main-content container">
  <div class="row">
    <div class="col-sm-12">

    <!-- SLIDER -->
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
      <ol class="carousel-indicators">
        <?php

        /* PALLINI SLIDER
        --------------------------------------*/
          $qd_counter = 0;
          // La Query
          $qd_the_query = new WP_Query('category_name=chi-siamo&posts_per_page=3');

          // Il Loop
          while ( $qd_the_query->have_posts() ) : $qd_the_query->the_post();

          $qd_counter ++;
        ?>
        <li data-target="#carouselExampleIndicators" data-slide-to="<?php echo $qd_counter;?>" class="<?php if ($qd_counter == 1){ echo 'active'; } ?>"></li>
        <?php
          endwhile;

          // Ripristina Query & Post Data originali
          wp_reset_query();
          wp_reset_postdata();
        ?>
      </ol>

      <div class="carousel-inner">
        <?php

        /* SLIDER
        --------------------------------------*/
          $qd_counter = 0;
          // La Query
          $qd_the_query = new WP_Query('category_name=chi-siamo&cat=-50&posts_per_page=3');

          // Il Loop
          while ( $qd_the_query->have_posts() ) : $qd_the_query->the_post();

            $qd_counter ++;

              $qd_image_attributes = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'qd_big');
            ?>
        <div class="carousel-item  <?php if ($qd_counter == 1){ echo 'active'; } ?>">
          <img class="d-block w-100" src="<?php echo $qd_image_attributes[0]; ?>" style="background-size: cover; background-position: center center;" alt="First slide" />
        </div>
        <?php
          endwhile;
          // Ripristina Query & Post Data originali
          wp_reset_query();
          wp_reset_postdata();
        ?>
      </div>

      <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>

    </div>
  </div>

  <div class="row m_top">
    <div class="col-sm-12 text-center">
      <?php

        // La Query
        $qd_the_query = new WP_Query( 'page_id=73' );

        // Il Loop
        while ( $qd_the_query->have_posts() ) : $qd_the_query->the_post();
      ?>
      <?php the_content(); ?>
      <!-- <h1><?php //the_title(); ?></h1>
      <h2 class="subline">

      </h2-->

      <?php
        endwhile;

        // Ripristina Query & Post Data originali
        wp_reset_query();
        wp_reset_postdata();
      ?>

    </div>

  </div>
</div>

<!-- SEZIONE ABOUT -->

<div class="soci-quote">
  <div class="container">
    <div class="row">
      <div class="col-sm-5">
        <img src="<?php echo get_template_directory_uri().'/img/soci/lazzari_bw.jpg';?>" title="Giampiero Lazzari" alt="Giampiero Lazzari">
      </div>
      <div class="col-sm-7">
        <hr>
        <h3><b>GIAMPIERO LAZZARI</b></h3>
        <h4 style="font-weight:300"><?php esc_html_e('managing director','qd') ?></h4>
        <h4><i><?php esc_html_e( '"Nulla è impossibile per colui che osa" Alessandro Magno', 'qd') ?></i></h4>
        <p><?php esc_html_e( 'La ricerca continua del bello e del lusso, l&rsquo;amore per i viaggi e il profondo interesse per le culture straniere hanno contribuito a formare la personalit&agrave; poliedrica e trasversale di Giampiero Lazzari, anima direzionale e organizzativa di Four Dimensions. L&rsquo;esperienza decennale nel General Contractor ha fatto emergere la sua attitudine manageriale e l&rsquo;abilit&agrave; nell&rsquo;affrontare la responsabilit&agrave; operativa complessiva di un progetto. Perfezionista assoluto, dedito al lavoro e alla ricerca costante con l&rsquo;obiettivo di affinare il proprio operato, rappresenta un punto fermo per la societ&agrave;. Il suo approccio pratico esprime concretezza e allo stesso tempo interesse per l&rsquo;estetica e per l&rsquo;immagine, con un&rsquo;attenzione alla cura dei minimi dettagli di ogni singolo progetto.', 'qd') ?></p>				</div>
    </div>
    <div class="row">
      <div class="col-sm-5">
        <img src="<?php echo get_template_directory_uri().'/img/soci/bicego_bw.jpg';?>" title="Andrea Bicego" alt="Andrea Bicego">
      </div>
      <div class="col-sm-7">
        <hr>
        <h3><b>ANDREA BICEGO</b></h3>
        <h4 style="font-weight:300"><?php esc_html_e('architect', 'qd') ?></h4>
        <h4><i><?php esc_html_e( '"Gli uomini comuni guardano le cose nuove con occhio vecchio. L’uomo creativo osserva le cose vecchie con occhio nuovo." Gian Piero Bona', 'qd') ?></i></h4>
        <p><?php esc_html_e( 'Ascoltare, immaginare, progettare e infine disegnare, per dare corpo e anima alle esigenze dei clienti: Andrea Bicego ha la facolt&agrave; di trasferire i desideri sulla carta, rispondendo con precisione a qualsiasi necessit&agrave;. Attraverso il suo know-how, i progetti di Four Dimensions prendono forma, veicolati dalle conoscenze tecniche che completano la sua formazione. Una professionalit&agrave; trasversale, in cui fantasia e precisione si fondono, per dare vita a soluzioni uniche e originali. Plasmando gli spazi, d&agrave; forma alle necessit&agrave; e ai desideri dei clienti di avere un luogo dove abitare, rifugiarsi in solitudine o incontrare altri, lavorare, divertirsi, vendere, studiare o giocare.', 'qd') ?></p>				</div>
    </div>
    <div class="row">
      <div class="col-sm-5">
        <img src="<?php echo get_template_directory_uri().'/img/soci/marelli_bw.jpg';?>" title="Barbara Marelli" alt="Barbara Marelli">
      </div>
      <div class="col-sm-7">
        <hr>
        <h3><b>BARBARA MARELLI</b></h3>
        <h4 style="font-weight:300"><?php esc_html_e('architect', 'qd') ?></h4>
        <h4><i><?php esc_html_e( '"L’arte non è il bello ma vedere le cose in maniera diversa." Virginia Woolf', 'qd') ?></i></h4>
        <p><?php esc_html_e( 'Unico architetto donna del team, Barbara Marelli conferisce un tocco di femminile creativit&agrave; a Four Dimensions. Affronta ogni lavoro con un atteggiamento che colloca al centro di ogni azione &quot;l&#39;Uomo&quot;, con i suoi bisogni, i suoi valori i suoi desideri. Creativa e attenta ad ogni dettaglio, si pone un preciso obiettivo: realizzare il sogno nel cassetto di ogni cliente, per restituire loro la casa in cui hanno sempre desiderato vivere. L&rsquo;accurata ricerca dei materiali costituisce il valore aggiunto del suo lavoro, contribuendo a definire l&rsquo;identit&agrave; dei progetti: uno degli elementi indispensabili per ottenere soluzioni uniche e potenziali di innovazione, in grado di generare nuovi contesti sensoriali e linguaggi espressivi.', 'qd') ?></p>				</div>
    </div>
    <div class="row">
      <div class="col-sm-5">
        <img src="<?php echo get_template_directory_uri().'/img/soci/bigoni_bw.jpg';?>" title="Massimo Bigoni" alt="Massimo Bigoni">
      </div>
      <div class="col-sm-7">
        <hr>
        <h3><b>MASSIMO BIGONI</b></h3>
        <h4 style="font-weight:300"><?php esc_html_e('architect', 'qd') ?></h4>
        <h4><i><?php esc_html_e( '"Dove c’è una grande volontà non possono esserci grandi difficoltà" Niccolò Machiavelli', 'qd') ?></i></h4>
        <p><?php esc_html_e( 'Concreto, pratico, pragmatico, Massimo Bigoni &egrave; colui che, in Four Dimensions, si occupa della parte tecnica e di edilizia. Come architetto, ci&ograve; che &egrave; per lui di imprescindibile importanza &egrave; la dimensione funzionale dell&rsquo;architettura che, se ben progettata, incide concretamente sulla qualit&agrave; dell&rsquo;abitare e, di conseguenza, sulla vita di ognuno di noi. Ingegnerizza i progetti, lavorando principalmente sulle costruzioni ex novo e sulle grandi ristrutturazioni, coordinando le maestranze. La sua solidit&agrave; si rispecchia nei suoi lavori, garanzia di sicurezza, stabilit&agrave; e consistenza.', 'qd') ?></p>				</div>
    </div>
  </div>
</div>

<div class="container">
  <h2 class="upp centered m_top"><?php esc_html_e('team','qd') ?></h2>
  <div class="row">

    <div class="col-sm-3">
      <div class="division">
        <h5 class="upp centered"><?php esc_html_e('general management', 'qd') ?></h5>
      </div>
    </div>

    <div class="col-sm-3">
      <div class="division">
        <h5 class="upp centered"><?php esc_html_e('commercial division', 'qd') ?></h5>
      </div>
    </div>

    <div class="col-sm-3">
      <div class="division">
        <h5 class="upp centered"><?php esc_html_e('project division', 'qd') ?></h5>
      </div>
    </div>

    <div class="col-sm-3">
      <div class="division">
        <h5 class="upp centered"><?php esc_html_e('graphic division', 'qd') ?></h5>
      </div>
    </div>

  </div>

  <div class="row d-none d-sm-flex">

    <div class="col-sm-3 m_top10">
      <div class="sector">
        <p><?php esc_html_e('Business Development and Strategy', 'qd') ?></p>
      </div>
    </div>

    <div class="col-sm-3 m_top10">
      <div class="sector">
        <p><?php esc_html_e('Administration and Accounting', 'qd') ?></p>
        <p><?php esc_html_e('Back Office', 'qd') ?></p>
      </div>
    </div>

    <div class="col-sm-3 m_top10">
      <div class="sector">
        <p><?php esc_html_e('Architecture Team', 'qd') ?></p>
        <p><?php esc_html_e('Interior Design Team', 'qd') ?></p>
        <p><?php esc_html_e('Engineering Team', 'qd') ?></p>
        <p><?php esc_html_e('RND Team', 'qd') ?></p>
        <p><?php esc_html_e('Landscaping Team', 'qd') ?></p>
      </div>
    </div>

    <div class="col-sm-3 m_top10">
      <div class="sector">
        <p><?php esc_html_e('Rendering Team' ,'qd') ?></p>
        <p><?php esc_html_e('Image', 'qd') ?></p>
        <p><?php esc_html_e('Graphic Communication', 'qd') ?></p>
      </div>
    </div>
  </div>
</div>

<?php get_footer(); ?>
