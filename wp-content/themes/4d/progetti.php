<?php
/*
 Template Name: progetti
*/
  get_header();
?>

  <div class="main-content container">
    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

      <div class="row mt-4">
      <?php
    		$catquery = new WP_Query('category_name=progetti');
    		if (have_posts()) :	while($catquery->have_posts()) : $catquery->the_post();
	    ?>
        <div class="col-sm-4 m_bot">
          <div class="project">
            <h5 class="upp"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h5>
            <a href="<?php the_permalink(); ?>">
              <?php the_post_thumbnail('', array('class' => 'img-fluid','alt' => get_the_title())); ?>
            </a>
            <p>
              <?php $luogo = get_post_custom_values( 'luogo' );
              foreach ( $luogo as $key => $value ) {
                echo "$value";
              } ?>
            </p>
          </div>
        </div>


          <?php endwhile;
      wp_reset_postdata();
  ?>
      </div>

    </article>
    <div class="pagination">

    <?php /* Pagination */
      global $wp_query;
      $big = 999999999; // need an unlikely integer
      echo paginate_links( array(
        'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
        'format' => '?paged=%#%',
        'current' => max( 1, get_query_var('paged') ),
        'total' => $wp_query->max_num_pages
      ) );
    ?>
    </div>

    <?php else : ?>

    <h3> <?php esc_html_e('Spiacente, non ci sono post per la tua ricerca.', 'qd'); ?> </h3>

    <?php endif; ?>

  </div>

  <?php get_footer(); ?>
