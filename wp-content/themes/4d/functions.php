<?php

// Dipendenze
require_once('assets/bs4navwalker.php');

/*  Theme setup
/* ------------------------------------ */
if ( ! function_exists('qd_setup')) {
  function qd_setup_theme(){

    add_theme_support("title-tag"); // supporto per i titoli

    add_theme_support("post-thumbnails"); // attivazione immagini in evidenza per articoli e pagine

    // setup dimensioni custom immagini
    add_image_size('qd_big', 1400, 800, true);
    add_image_size('qd_quad', 600, 600, true);
    add_image_size('qd_single', 800, 500, true);

    // setup menu
    register_nav_menus(array(
      'header' => esc_html__('Header', 'qd'),
      'footer' => esc_html__('Footer', 'qd' ),
      'lang' => esc_html__('Lang', 'qd' )
    ));

    // load theme languages
    load_theme_textdomain( 'qd', get_template_directory().'/languages');

  }
}

add_action('after_setup_theme', 'qd_setup_theme');


/* Include javascript files

-------------------------------------------------------- */
if ( ! function_exists( 'qd_scripts' ) ) {
  function qd_scripts() {

    //JS
    wp_enqueue_script( 'jquery' );
    wp_enqueue_script( 'qd-popper', 'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js', array( 'jquery' ),null, true );
    wp_enqueue_script( 'qd-bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"', array( 'jquery' ),null, true );
    // wp_enqueue_script( 'qd-scripts', get_template_directory_uri().'/js/scripts.js', array( 'jquery' ),'', true );
  }
}

add_action('wp_enqueue_scripts', 'qd_scripts');


/* Include css files
-------------------------------------------------------- */

if(! function_exists('qd_style') ) {
  function qd_style(){

    //CSS
    wp_enqueue_style( 'qd-bootstrap-css', 'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css');
    wp_enqueue_style( 'qd-normalize-css', 'https://necolas.github.io/normalize.css/8.0.0/normalize.css');
    wp_enqueue_style( 'qd-style-default-css', get_template_directory_uri() .'/style.css');
    wp_enqueue_style( 'qd-font-awesome', 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css');
    wp_enqueue_style( 'qd-font','https://fonts.googleapis.com/css?family=Lato:300,400,700');
	}
}
add_action( 'wp_enqueue_scripts', 'qd_style' );



?>
