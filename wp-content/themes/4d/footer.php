
<footer>
	<div class="footer-up d-none d-sm-none d-md-block">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 text-center">
          <div class="link" id="bs4navbars">
            <?php
              wp_nav_menu([
                'menu'           => 'footer',
                'theme_location' => 'footer',
                'container'       =>  false,
                'container_class' => 'link',
                'menu_id'         => false,
                'menu_class'      => 'nav navbar-nav menu-footer',
                'depth'           => 2,
                'fallback_cb'     => 'bs4navwalker::fallback',
                'walker'          => new bs4navwalker()
              ]);
            ?>
          </div>
				</div>
			</div>
		</div>
	</div>
	<div class="footer-dw">
		<div class="container">
			<div class="row">
				<div class="col-sm-8 m_bot10">
					<p class="m_top10"><small><?php esc_html_e ('Four Dimensions S.R.L. - Via Risorgimento, 13 - 22063 Cantù (CO), Italy - P.I. IT03440250136', 'qd') ?></small></p>
				</div>
				<div class="col-sm-4">
					<p class="m_top10">development <a href="https://www.linkedin.com/in/lorenzo-veneziani-572a71165/" title="Lorenzo Veneziani" target="_blank">Lorenzo Veneziani</a></small>
				</div>
			</div>
		</div>
	</div>
</footer>

<?php wp_footer(); ?>

</body>
</html>
