<?php get_header(); ?>

<div class="main-content">

<!-- SLIDER -->
  <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
      <?php

      /* PALLINI SLIDER
      --------------------------------------*/
        $qd_counter = 0;
        // La Query
        $qd_the_query = new WP_Query('category_name=in-evidenza');

        // Il Loop
        while ( $qd_the_query->have_posts() ) : $qd_the_query->the_post();

        $qd_counter ++;
      ?>
      <li data-target="#carouselExampleIndicators" data-slide-to="<?php echo $qd_counter;?>" class="<?php if ($qd_counter == 1){ echo 'active'; } ?>"></li>
      <?php
        endwhile;

        // Ripristina Query & Post Data originali
        wp_reset_query();
        wp_reset_postdata();
      ?>
    </ol>

    <div class="carousel-inner">
      <?php

      /* SLIDER
      --------------------------------------*/

        $qd_counter = 0;
        // La Query
        $qd_the_query = new WP_Query('category_name=in-evidenza');

        // Il Loop
        while ( $qd_the_query->have_posts() ) :
          $qd_the_query->the_post();

          $qd_counter ++;

            $qd_image_attributes = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'qd_big');
          ?>
      <div class="carousel-item  <?php if ($qd_counter == 1){ echo 'active'; } ?>">
        <img class="d-block w-100" src="<?php echo $qd_image_attributes[0]; ?>" style="background-size: cover; background-position: center center;" alt="First slide" />
        <div class="carousel-caption box d-none d-md-block">
          <h4><?php the_title(); ?></h4>
        </div>
      </div>
      <?php
        endwhile;

        // Ripristina Query & Post Data originali
        wp_reset_query();
        wp_reset_postdata();
      ?>
    </div>

    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>

  <div class="container">

    <?php

    /* INTRO
    --------------------------------------*/

      // La Query
      $qd_the_query = new WP_Query('page_id=120');

      // Il Loop
      while ( $qd_the_query->have_posts() ) : $qd_the_query->the_post();
    ?>
    <div class="row m_top">
      <div class="col-sm-12 text-center">
        <h2 class="subline"><?php the_content(); ?></h2>
      </div>
    </div>
    <?php
      endwhile;

      // Ripristina Query & Post Data originali
      wp_reset_query();
      wp_reset_postdata();
    ?>

    <!-- SERVIZI
    -------------------------------------->
    <div class="col-sm-12 mb-3">
      <h1 class="display-4 text-center upp"><?php esc_html_e( 'servizi', 'qd' ) ?></h1>
    </div>

    <div class="row">
      <div class="col-sm-12">
        <div class="mod_projects">
          <img src="<?php echo get_template_directory_uri().'/img/home/bg_projects_l.png'; ?>" style="position:absolute;" class="d-none d-xl-block" alt="Four Dimensions" title="Four Dimensions">
          <img src="<?php echo get_template_directory_uri().'/img/home/bg_projects_m.png'; ?>" style="position:absolute;" class="d-none d-lg-block d-xl-none" alt="Four Dimensions" title="Four Dimensions">
          <img src="<?php echo get_template_directory_uri().'/img/home/bg_projects_s.png'; ?>" style="position:absolute;" class="d-none d-md-block d-lg-none" alt="Four Dimensions" title="Four Dimensions">

          <div class="row q-superiore">
            <div class="col-sm-4 offset-sm-2 mb-20">
              <?php if (get_locale() == 'en_GB')
              { echo "<a href='services#architecture'>"; }
                elseif (get_locale() == 'it_IT')
              { echo "<a href='servizi#architecture'>"; }
                elseif (get_locale() == 'ru_RU')
              { echo "<a href='услуги#architecture'>"; }
              ?>
                <div class="box m_bot10">
                  <img src="<?php echo get_template_directory_uri().'/img/home/architecture.jpg';?>" class="img-fluid" title="architecture" alt="architecture">
                </div>
              </a>
            </div>

            <div class="col-sm-4 mb-20 box-dx">
              <?php if (get_locale() == 'en_GB')
              { echo "<a href='services#design'>"; }
                elseif (get_locale() == 'it_IT')
              { echo "<a href='servizi#design'>"; }
                elseif (get_locale() == 'ru_RU')
              { echo "<a href='услуги#design'>"; }
              ?>
                <div class="box m_bot10">
                  <img src="<?php echo get_template_directory_uri().'/img/home/interior-design.jpg'; ?>" class="img-fluid" title="interior-design" alt="interior-design">
                </div>
              </a>
            </div>
          </div>

          <div class="row q-inferiore">
            <div class="col-sm-4 offset-sm-2 mb-20">
              <?php if (get_locale() == 'en_GB')
              { echo "<a href='services#contract'>"; }
                elseif (get_locale() == 'it_IT')
              { echo "<a href='servizi#contract'>"; }
                elseif (get_locale() == 'ru_RU')
              { echo "<a href='услуги#contract'>"; }
              ?>
                <div class="box m_bot10">
                  <img src="<?php echo get_template_directory_uri().'/img/home/contract.jpg'; ?>" class="img-fluid" title="contract" alt="contract">
                </div>
              </a>
            </div>

            <div class="col-sm-4 mb-20 box-dx">
              <?php if (get_locale() == 'en_GB')
              { echo "<a href='services#consulting'>"; }
                elseif (get_locale() == 'it_IT')
              { echo "<a href='servizi#consulting'>"; }
                elseif (get_locale() == 'ru_RU')
              { echo "<a href='услуги#consulting'>"; }
              ?>
                <div class="box m_bot10">
                  <img src="<?php echo get_template_directory_uri().'/img/home/consulting.jpg'; ?>" class="img-fluid" title="consulting" alt="consulting">
                </div>
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="d-none d-md-block mt6"></div>

    <div class="row">

      <div class="col-sm-12 mb-3">
        <h1 class="display-4 text-center upp"><?php esc_html_e( 'i soci', 'qd' ) ?></h1>
      </div>
      <?php

      /* SOCI
      --------------------------------------*/

        // La Query
        $qd_the_query = new WP_Query('category_name=focus&posts_per_page=4');

        // Il Loop
        while ( $qd_the_query->have_posts() ) : $qd_the_query->the_post();
      ?>

      <div class="col-sm-3 text-center">
        <div class="box m_bot10">
  			  <?php the_post_thumbnail('qd_single', array('class' => 'img-fluid','alt' => get_the_title())); ?>
          <h4 class="nome"><?php the_title(); ?></h4><span class="text"><?php the_content(); ?></span>
        </div>
      </div>

      <?php
        endwhile;

        // Ripristina Query & Post Data originali
        wp_reset_query();
        wp_reset_postdata();
      ?>
    </div>

  </div>        <!-- FINE CONTAINER -->
</div>        <!-- FINE MAIN CONTENT -->


<?php  get_footer(); ?>
