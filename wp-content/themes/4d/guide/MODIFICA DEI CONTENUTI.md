# MODIFICA DEI CONTENUTI

#### HOME PAGE

- __SLIDER__: 
	-  Andare nella sezione ___Articoli___;
	-  Nella lista di articoli, individuare la categoria "_in evidenza_" e cliccarci sopra per visualizzare solo gli articoli appartenenti a quella categoria;
	-  Cliccare sul nome dell'immagine da modificare ---> ___esempio: mosca, russia___;
	-  Modificare, se necessario, il titolo. In basso c'è l'immagine che è presente nello slider: per modificarla cliccare su '___rimuovi___' e quindi su '___imposta immagine___';
	-  Una volta fatto ciò, cliccare sul tasto "aggiorna";
	-  Ricordarsi di effettuare la modifica per tutte le lingue.

- __SCRITTA SOTTO SLIDER__:
	
	- Andare nella sezione ___Pagine___;
	- Trovare la pagina con titolo ___intro home___ + lingua ---> ___esempio: intro home it___;
	- Modificare il testo;
	- Una volta fatto ciò, cliccare sul tasto "aggiorna";
	
- __SOCI__: 
	- Volendo si può modificare anche questa sezione, ma non credo sia necessario.

- __SIMBOLO FOUR DIMENSIONS__: 
	- ???????????


#### CHI SIAMO

- __SLIDER__: È possibile modificare le immagini dello slider con un processo analogo a quello delle immagini in home page, ecco come:
	-  Andare nella sezione ___Articoli___;
	-  Nella lista di articoli, individuare la categoria "_chi siamo_" e cliccarci sopra per visualizzare solo gli articoli appartenenti a quella categoria;
	-  Le immagini son ordinate numericamente, scegliere quindi quella da sostituire;
	-  In basso c'è l'immagine che è presente nello slider: per modificarla cliccare su '___rimuovi___' e quindi su '___imposta immagine___';
	-  	Una volta fatto ciò, cliccare sul tasto "aggiorna";
	-  Ricordarsi di effettuare la modifica per tutte le lingue.


#### NEWS

È possibile modificare gli articoli con un processo analogo a quello delle immagini dello slider in home page.