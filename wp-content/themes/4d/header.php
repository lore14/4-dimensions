<!DOCTYPE html>
<html <?php language_attributes(); ?> >
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
		<title><?php bloginfo('title'); ?></title>
		<meta name="description" content="<?php bloginfo('description'); ?>">
		<meta name="keywords" content="">
		<script src='https://www.google.com/recaptcha/api.js?render=6LdJkYEUAAAAAF6VFwH_j0NTHxSNYQsSxuAtB0R5'></script>

    <?php wp_head(); ?>

	</head>
	<body <?php body_class(); ?>>

	<header>
		<div class="container">
			<div class="row m_top">
				<div class="col-sm-3 d-none d-lg-block">
					<a href="<?php echo esc_url_raw(home_url()); ?>">
						<img src="<?php echo get_template_directory_uri(); ?>/img/logo.png" alt="logo sito;">
					</a>
				</div>
				<div class="col-sm-3 sgamo"></div>
				<div class="col-sm-9 text-right">

					<?php
					 wp_nav_menu([
						 'menu'            => 'lang',
						 'theme_location'  => 'lang',
						 'container'       =>  false,
						 'container_id'    => '',
						 'container_class' => 'collapse navbar-collapse',
						 'menu_id'         => false,
						 'menu_class'      => 'nav navbar-nav',
						 'depth'           => 2,
						 'fallback_cb'     => 'bs4navwalker::fallback',
						 'walker'          => new bs4navwalker()
					 ]);
				 ?>
        </div>


				<div class="col-sm-12">
					<nav class="navbar navbar-expand-lg navbar-light navbar-default">
            <div class="navbar-header">

							 <!--   ICONA HAMBURGER   -->
							<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#bs4navbar" aria-controls="main_menu" aria-expanded="false" aria-label="Toggle navigation">
				       	<span class="navbar-toggler-icon"></span>
					   	</button>
							<!--   FINE ICONA   -->

              <div class="pull-right">
                  <a class="navbar-brand d-lg-none" href="<?php echo esc_url_raw(home_url()); ?>">
									<img src="<?php echo get_template_directory_uri(); ?>/img/logo.png" title="4 Dimensions" alt="4 Dimensions" height="30px"></a>
              </div>
            </div>

            <div class="collapse navbar-collapse" id="bs4navbar">
							<?php
					     wp_nav_menu([
					       'menu'            => 'header',
					       'theme_location'  => 'header',
					       'container'       =>  false,
					       'container_id'    => '',
					       'container_class' => 'collapse navbar-collapse',
					       'menu_id'         => false,
					       'menu_class'      => 'nav navbar-nav',
					       'depth'           => 2,
					       'fallback_cb'     => 'bs4navwalker::fallback',
					       'walker'          => new bs4navwalker()
					     ]);
					   ?>

					</nav>
				</div>

			</div>
		</div>
	</header>
