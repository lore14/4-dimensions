<?php
/*
 Template Name: contatti
*/
  get_header();
?>


<div class="main-content">

  <div class="contact-box m_bot">
    <div class="container">
      <div class="row m_top">
        <div class="col-sm-12 text-center">
          <h1>FOUR DIMENSIONS SRL</h1>
            <p><br>P.zza Garibaldi 13 - 22063 Cantù (Co)<br>Tel. +39 031 705804 . Fax +39 031 2280529<br><a style="margin-right:15px;" href="info@4dimensions.it">info@4dimensions.it</a></p>
        </div>
      </div>
    </div>
  </div>


  <?php if (have_posts()) : while(have_posts()) : the_post(); ?>

  <div class="container">
    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

    <div class="row m_top">
      <div class="col-sm-12 text-center">
        <h1 class="text-center upp"><?php esc_html_e( 'Contattaci','qd') ?></h1>
        <!-- <h1 class="upp"><?php //the_title(); ?></h1> -->
        <?php the_content();?>
        <!-- <h2 class="subline"></h2> -->
        <script>
        grecaptcha.ready(function() {
          grecaptcha.execute('6LdJkYEUAAAAAF6VFwH_j0NTHxSNYQsSxuAtB0R5', {action: 'action_name'})
          .then(function(token) {
          // Verify the token on the server.
          });
        });
</script>
      </div>
    </div>
  </div>

  <div class="contact-box map m_top">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2784.5654785872043!2d9.128890000000002!3d45.7398083!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4786991cdaf95afb%3A0xefe1eb2c731d0902!2sPiazza+Giuseppe+Garibaldi%2C+13%2C+22063+Cant%C3%B9+CO!5e0!3m2!1sit!2sit!4v1443011302108" height="450" style="border:0;width:100%" allowfullscreen></iframe>
				</div>
			</div>
		</div>
	</div>



  </article>

  <?php endwhile; ?>

    <div class="pagination">

      <?php /* Pagination */
      global $wp_query;
      $big = 999999999; // need an unlikely integer
      echo paginate_links( array(
        'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
        'format' => '?paged=%#%',
        'current' => max( 1, get_query_var('paged') ),
        'total' => $wp_query->max_num_pages
      ) );
      ?>

    </div>

  <?php else : ?>

    <h3> <?php esc_html_e('Spiacente, non ci sono post per la tua ricerca.', 'qd'); ?> </h3>

  <?php endif; ?>


</div>

<?php get_footer(); ?>
