<?php get_header(); ?>

<div class="main-content container">
	<?php if (have_posts()) :?><?php while(have_posts()) : the_post(); ?>

		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

			<div class="row">
				<div class="col-sm-12">
					<h1 class="display-4 text-center"><?php the_title(); ?></h1>

					<p> <?php the_time('j M , Y') ?></p>
				</div>
			</div>

			<div class="row m_bot">
				<div class="col-sm-12">
					<?php the_post_thumbnail('', array('class' => 'img-fluid','alt' => get_the_title())); ?>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-12 news-text">
					<?php the_content();?>
				</div>
			</div>

      <hr>

      <?php comments_template(); ?>

		</article>

	<?php endwhile; ?>

		<div class="pagination">

			<?php /* Pagination */
			global $wp_query;
			$big = 999999999; // need an unlikely integer
			echo paginate_links( array(
				'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
				'format' => '?paged=%#%',
				'current' => max( 1, get_query_var('paged') ),
				'total' => $wp_query->max_num_pages
			) );
			?>

		</div>

	<?php else : ?>

	  <h3> <?php esc_html_e('Spiacente, non ci sono post per la tua ricerca.', 'qd'); ?> </h3>

	<?php endif; ?>

</div>

<aside class="sidebar">
		<?php get_sidebar(); ?>
</aside>


<?php get_footer(); ?>
